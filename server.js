const WebSocket = require('ws');

const wss = new WebSocket.Server({ port: 8080 });

wss.on('connection', (ws) => {
  console.log('Client connected');

  ws.on('message', (message, isBinary) => {
    console.log(`Received message: ${message}`);
    const sendMessage = isBinary ? message : message.toString();

    // Broadcast the message to all connected clients
    wss.clients.forEach((client) => {
      if (client.readyState === WebSocket.OPEN) {
        client.send(sendMessage);
      }
    });
  });

  ws.on('close', () => {
    console.log('Client disconnected');
  });
});
